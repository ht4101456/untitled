package cheapcopy.interceptor;

import cheapcopy.annotation.InterceptorEx;
import cheapcopy.controller.vo.ReturnT;
import com.google.gson.Gson;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * 重写默认异常拦截器
 */
@InterceptorEx
public class ExceptionInterceptor extends DefaultExceptionInterceptor {
    @Override
    public Object afterException(Object obj, Method method, Object[] args, MethodProxy proxy, Exception e) {
       return new Gson().toJson(new ReturnT(500, e.toString()));
    }
}
