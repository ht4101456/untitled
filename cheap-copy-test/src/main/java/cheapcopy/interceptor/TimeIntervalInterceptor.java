package cheapcopy.interceptor;

import cheapcopy.annotation.InterceptorEx;
import lombok.extern.slf4j.Slf4j;
import cheapcopy.proxy.BasicAspect;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * 执行时间拦截器
 */
@Slf4j
@InterceptorEx(order = -99)
public class TimeIntervalInterceptor extends BasicAspect {

    private long taskRecord = 0L;

    @Override
    public boolean before(Object obj, Method method, Object[] args, MethodProxy proxy) {

        taskRecord = System.currentTimeMillis();
        return true;
    }

    @Override
    public void after(Object obj, Method method, Object[] args, MethodProxy proxy, Object returnVal) {
        if(!method.getName().equals("hashCode")||!method.getName().equals("toString")||!method.getName().equals("finalize"))
        log.info("Method [{}.{}] execute spend [{}]ms return value [{}]", obj.getClass().getName(), method.getName(), System.currentTimeMillis() - taskRecord, returnVal);
    }

}
