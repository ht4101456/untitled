package cheapcopy.controller.vo;

import java.io.Serializable;

/**
 * 返回协议
 */
public class ReturnT<T> implements Serializable {

    private static final long serialVersionUID = 362498820763181266L;

    private Integer code;
    private String message;
    private T data;

    public ReturnT(){}


    public ReturnT(Integer code,String message){
        this.code = code;
        this.message = message;
    }

    public ReturnT<T> ok(){
        this.code = 200;
        this.message = "服务器响应成功！";
        return this;
    }

    public ReturnT<T> error(){
        this.code = 500;
        this.message = "服务器响应失败！";
        return this;
    }

    public ReturnT<T> setData(T data) {
        this.data = data;
        return this;
    }
}
