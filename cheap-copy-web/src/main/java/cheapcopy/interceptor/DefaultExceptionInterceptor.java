package cheapcopy.interceptor;


import cheapcopy.annotation.InterceptorEx;
import net.sf.cglib.proxy.MethodProxy;
import cheapcopy.proxy.BasicAspect;

import java.lang.reflect.Method;

/**
 * 异常后置拦截器 默认
 */
@InterceptorEx
public class DefaultExceptionInterceptor extends BasicAspect {

    @Override
    public Object afterException(Object obj, Method method, Object[] args, MethodProxy proxy, Exception e) {
        return "调用失败,错误信息:"+e.getMessage();
    }
}
