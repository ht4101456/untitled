package cheapcopy.interceptor;


import cheapcopy.annotation.InterceptorEx;
import cheapcopy.proxy.BasicAspect;
import cheapcopy.util.ListUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * 日志拦截器
 */
@Slf4j
@InterceptorEx(order = -97)
public class DefaultLogInterceptor extends BasicAspect {
    @Override
    public boolean before(Object obj, Method method, Object[] args, MethodProxy proxy) {
        if(!method.getName().equals("hashCode")||!method.getName().equals("toString")||!method.getName().equals("finalize"))
        log.info("调用方法[{}],参数[{}]",method.getName(), ListUtil.parseList(args));
        return true;
    }
}
