package cheapcopy.interceptor;

import cheapcopy.proxy.Aspect;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * 默认拦截器
 */
public class DefaultInterceptors {
    private static List<Aspect> interceptors;
    public static List<Aspect> getDefaultInterceptor(){
        if(interceptors==null) {
            interceptors = Lists.newArrayList();
            //TODO 这儿得来个配置化
            interceptors.add(new DefaultExceptionInterceptor());
            interceptors.add(new DefaultLogInterceptor());
        }
        return interceptors;
    }
}
