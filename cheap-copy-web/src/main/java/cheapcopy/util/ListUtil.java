package cheapcopy.util;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListUtil {
    public static List<String> parseList(Object[] args){
        ArrayList<String> lists = Lists.newArrayList();
        Arrays.stream(args).forEach(
                arg->{
                    lists.add(String.valueOf(arg));
                }
        );
        return lists;
    }
}
