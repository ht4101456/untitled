package cheapcopy.server;



import cheapcopy.proxy.Aspect;

import java.util.HashMap;
import java.util.LinkedList;

public class ServletContext {
    /**
     * 拦截器集合
     */
    private static HashMap<String,Object> interceptors = new HashMap<>();

    /**
     * 处理后的拦截器集合
     */
    public static volatile LinkedList<Aspect> invokeAspects = null;

    /**
     * 映射器集合
     */
    private static HashMap<String,Class<?>> servlets = new HashMap<>();

    public static HashMap<String, Class<?>> getServlets() {
        return servlets;
    }

    public static void addServletNode(String key,Class<?> classz) {
        servlets.put(key,classz);
    }

    public static HashMap<String, Object> getInterceptors() {
        return interceptors;
    }
    public static <T extends Aspect> void addInterceptorNode(String key, T aspect) {
        interceptors.put(key,aspect);
    }
    public static void  setAspects(LinkedList<Aspect> aspects){
        invokeAspects = aspects;
    }

}
